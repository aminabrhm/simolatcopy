import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { NonSaudiPageRoutingModule } from './non-saudi-routing.module';

import { NonSaudiPage } from './non-saudi.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    NonSaudiPageRoutingModule
  ],
  declarations: [NonSaudiPage]
})
export class NonSaudiPageModule {}
