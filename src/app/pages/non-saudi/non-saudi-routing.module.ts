import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { NonSaudiPage } from './non-saudi.page';

const routes: Routes = [
  {
    path: '',
    component: NonSaudiPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class NonSaudiPageRoutingModule {}
