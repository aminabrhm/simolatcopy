import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { NonSaudiPage } from './non-saudi.page';

describe('NonSaudiPage', () => {
  let component: NonSaudiPage;
  let fixture: ComponentFixture<NonSaudiPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NonSaudiPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(NonSaudiPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
