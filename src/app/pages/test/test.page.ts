import { Component, OnInit } from '@angular/core';
import { NavController, ModalController } from '@ionic/angular';
import { User } from 'src/app/models/user';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-test',
  templateUrl: './test.page.html',
  styleUrls: ['./test.page.scss'],
})
export class TestPage implements OnInit {

  constructor(
    private authService: AuthService,
  ) { }
  user: User;
  user_type: string;

  ngOnInit() {
  }

  
  ionViewWillEnter() {
    this.authService.user().subscribe(
      user => {
        this.user = user;
        this.user_type = this.user.user_type
      }
    );
  }

}

// import { Component, OnInit } from '@angular/core';
// import { SoicalinfoService } from './../../../services/soicalinfo.service';
// import { AlertService } from 'src/app/services/alert.service';
// import { AuthService } from 'src/app/services/auth.service';
// import { Subscription } from 'rxjs';
// import { HttpClient, HttpHeaders } from '@angular/common/http';
// import { EnvService } from './../../../services/env.service';
// import * as moment from 'moment';
// import { Socialinfo } from 'src/app/models/socialinfo';
// import {HttpServiceService} from './../../../services/http-service.service'

// @Component({
//   selector: 'app-social-info',
//   templateUrl: './social-info.page.html',
//   styleUrls: ['./social-info.page.scss'],
// })
// export class SocialInfoPage implements OnInit {
//   social_info: Socialinfo;

//   constructor(
//     private soicalInfoService: SoicalinfoService,
//     private alertService: AlertService,
//     private authService :AuthService,
//     private http : HttpClient,
//     private env: EnvService,
//     private httpService: HttpServiceService
//   ) { }

//   ngOnInit() {
//   }
  
//   // ionViewWillEnter() {
//   //   this.httpService.makeGet('auth/createsocial').subscribe(
//   //     social_info => {
//   //       this.social_info = social_info;
//   //     },
//   //     error => {
//   //       console.log(error, "error ionViewWillEnter");
//   //     },
//   //     () => {
        
//   //     }
//   //   );
//   // }



//   logForm() {
//     // this.social_info.disease_date = moment(this.social_info.disease_date).format("YYYY-MM-DD");

//     let data = {
//       social_info: this.social_info
//     }
//     this.soicalInfoService.store(data).subscribe(
//       data => {
//         this.alertService.presentToast("تم حفظ البيانات بنجاح");
//         console.log(data);
//       },
//       error => {
//         console.log(error);
//       },
//       () => {
        
//       }
//     )
//   }

// }
