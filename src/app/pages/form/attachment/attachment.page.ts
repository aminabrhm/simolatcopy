import { Component, OnInit } from '@angular/core';
import{ HttpServiceService}  from './../../../services/http-service.service'
import { AlertService } from 'src/app/services/alert.service';
import { EnvService } from './../../../services/env.service';
import { HttpClient ,HttpHeaders} from '@angular/common/http';
import { Camera } from '@ionic-native/camera';

@Component({
  selector: 'app-attachment',
  templateUrl: './attachment.page.html',
  styleUrls: ['./attachment.page.scss'],
})
export class AttachmentPage implements OnInit {
  imageURI:any;
  imageFileName:any;
  private file: File
  headers = new HttpHeaders();

  constructor(
    private httpService : HttpServiceService,
    private alertService: AlertService,
    private env: EnvService,
    private http : HttpClient,

    ) { }

  ngOnInit() {
    
  }

  onFileChange(fileChangeEvent){
    this.file=fileChangeEvent.target.files[0];
  }




  async submitForm(){
    let options = {headers: new HttpHeaders({ 
      'Content-Type': 'multipart/formdata',
     })  };
    let formData= new FormData();
    formData.append("photo",this.file, this.file.name);
    // console.log(formData)
    this.http.post<any>(this.env.API_URL + 'auth/form', formData, options).subscribe(
      data => {
        this.alertService.presentToast("تم رفع الملفات بنجاح");
      },
      error => {
        console.log(error);
      },
  )}


}
