import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { BankInfoPage } from './bank-info.page';

describe('BankInfoPage', () => {
  let component: BankInfoPage;
  let fixture: ComponentFixture<BankInfoPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BankInfoPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(BankInfoPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
