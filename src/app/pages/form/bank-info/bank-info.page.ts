import { Component, OnInit } from '@angular/core';
import { AlertService } from 'src/app/services/alert.service';
import { AuthService } from 'src/app/services/auth.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { EnvService } from './../../../services/env.service';
import { Observable } from 'rxjs';
import{ HttpServiceService}  from './../../../services/http-service.service'
import { Bank_info } from 'src/app/models/bank_info';

@Component({
  selector: 'app-bank-info',
  templateUrl: './bank-info.page.html',
  styleUrls: ['./bank-info.page.scss'],
})
export class BankInfoPage implements OnInit {

  bank_info: Bank_info;
  constructor(
    private alertService: AlertService,
    private authService :AuthService,
    private http : HttpClient,
    private env: EnvService,
    private httpService :HttpServiceService,
  ) { }

  ngOnInit() {
  }

  
  ionViewWillEnter() {
    this.httpService.get('auth/createbank').subscribe(
      bank_info => {
        this.bank_info = bank_info;
      },
      error => {
        console.log(error);
      },
      () => {
        
      }
    );
  }

  log(){
    console.log(this.bank_info)
  }


  logForm() {

    let data = {
      bank_info: this.bank_info
    }
    this.httpService.post(this.env.API_URL + 'auth/bank', data).subscribe(
      data => {
        this.alertService.presentToast("تم حفظ البيانات بنجاح");
      },
      error => {
        console.log(error);
      },
      () => {
        
      }
    )
  }

}
