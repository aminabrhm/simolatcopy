import { Component, OnInit } from '@angular/core';
import { AlertService } from 'src/app/services/alert.service';
import { AuthService } from 'src/app/services/auth.service';
import { Subscription } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { EnvService } from './../../../services/env.service';
import { Personalinfo } from 'src/app/models/personalinfo';
import {HttpServiceService} from './../../../services/http-service.service'
import * as moment from 'moment';
import { setOptions,hijriCalendar, localeAr } from '@mobiscroll/angular';

setOptions({
  theme: 'ios',
  themeVariant: 'light',
  display:'bottom',
  lang:'ar'
});


@Component({
  selector: 'app-personal-info',
  templateUrl: './personal-info.page.html',
  styleUrls: ['./personal-info.page.scss'],
})
export class PersonalInfoPage implements OnInit {
  public hijriCalendar = hijriCalendar;
  public localeAr = localeAr;
  
  personal_info: Personalinfo;
  constructor(
    private alertService: AlertService,
    private authService :AuthService,
    private http : HttpClient,
    private env: EnvService,
    private httpService: HttpServiceService

  ) { }
  ngOnInit() {
    console.log("Hello I'm Here");
  }

  ionViewWillEnter() {
    console.log('personal get fun')
    this.httpService.get('auth/createpersonalinfo').subscribe(
      personal_info => {
        this.personal_info = personal_info;
        if(!this.personal_info){
          this.personal_info = {
            first_name: '',
            second_name:'',
            third_name: '',
            family_name: '',
            gender: '',
            national_id: '',
            place_national: '',
            place_birth: '',
            date_birth: '',
            date_national: '',
            date_ex_national: '',
            job: '',
          };
        }
      },
      error => {
        console.log(error);
      },
    );
  }
  logForm() {
    console.log(this.personal_info)
    // this.personal_info.date_birth = moment(this.personal_info.date_birth).format("YYYY-MM-DD");
    // this.personal_info.date_national = moment(this.personal_info.date_national).format("YYYY-MM-DD");
    // this.personal_info.date_ex_national = moment(this.personal_info.date_ex_national).format("YYYY-MM-DD");
    let data = {
      personal_info: this.personal_info
    }
    this.authService.post('auth/personalInfo',data).subscribe(
      data => {
        this.alertService.presentToast("تم حفظ البيانات بنجاح");
        console.log(data);
      },
      error => {
        console.log(error);
      },
      () => {
      })}    
}
