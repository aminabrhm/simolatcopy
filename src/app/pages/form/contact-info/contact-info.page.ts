import { Component, OnInit } from '@angular/core';
import { ContactinfoService } from './../../../services/contactinfo.service';
import { AlertService } from 'src/app/services/alert.service';
import { AuthService } from 'src/app/services/auth.service';
import { Subscription } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { EnvService } from './../../../services/env.service';
import { Contact_info } from 'src/app/models/contact_info';
import { HttpServiceService } from './../../../services/http-service.service'

@Component({
  selector: 'app-contact-info',
  templateUrl: './contact-info.page.html',
  styleUrls: ['./contact-info.page.scss'],
})

// createcontact
export class ContactInfoPage implements OnInit {
  contact_info: Contact_info;

  constructor(
    private contactinfoService: ContactinfoService,
    private alertService: AlertService,
    private authService :AuthService,
    private http : HttpClient,
    private env: EnvService,
    private httpService: HttpServiceService

  ) {
    
   }

  ngOnInit() {
  }


  ionViewWillEnter() {
    this.httpService.get('auth/createcontact').subscribe(
      contact_info => {
        this.contact_info = contact_info;
        if(!this.contact_info){
          this.contact_info = {
            email: '',
            phone_number:'',
            mobile_number:'' ,
            whatsapp_number: '',
            other_number: '',
            relative: '',
          }
        }
      },
      error => {
        console.log(error);
      },
      () => {
        
      }
    );
  }

  logForm() {
    let data = {
      contact_info:this.contact_info
    }
    this.contactinfoService.store(data).subscribe(
      data => {
        this.alertService.presentToast("تم حفظ البيانات بنجاح");
      },
      error => {
        console.log(error);
      },
      () => {
      }
    )
  }

}
