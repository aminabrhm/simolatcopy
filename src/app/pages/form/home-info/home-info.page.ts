import { Component, OnInit } from '@angular/core';
import{ HttpServiceService}  from './../../../services/http-service.service'
import { AlertService } from 'src/app/services/alert.service';
import { AuthService } from 'src/app/services/auth.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { EnvService } from './../../../services/env.service';
import { ElementSchemaRegistry } from '@angular/compiler';
import { Location_info } from 'src/app/models/home_info';

@Component({
  selector: 'app-home-info',
  templateUrl: './home-info.page.html',
  styleUrls: ['./home-info.page.scss'],
})
export class HomeInfoPage implements OnInit {

  location_info : Location_info;
  noElectric :any;
  constructor(
    private httpService :HttpServiceService,
    private alertService: AlertService,
    private authService :AuthService,
    private http : HttpClient,
    private env: EnvService,
  ) { }

  ngOnInit() {
  }
  
  get(form, array){
    for(let entery of form) {      
      entery.isChecked === true 
      ? array.push(entery.val)
      : entery 
    }  
  }
  
  put(array, checkbox){
    checkbox.forEach(chk => chk.isChecked = array.includes(chk.val));
  }
  log(){
    console.log(this.location_info)
  }
  
  ionViewWillEnter() {

    this.httpService.get('auth/createloca').subscribe(
      location_info => {
        this.location_info = location_info;
        this.put(this.location_info.home_category, this.home_category)
        if(!this.location_info){
          this.location_info = {
            place: '',
            description:'',
            building_num:'' ,
            building_type: '',
            building_ownership: '',
            location_link: '',
            building_rent:'',
            rent_evaluate: '',
            room_number_all:'',
            room_number:'' ,
            home_status: '',
            status_reason: '',
            home_quality: '',
            bedroom_evaluate:'',
            kitchen_evaluate: '',
            bathroom_evaluate:'',
            store_evaluate:'' ,
            driverroom_evaluate: '',
            outdoor_evaluate: '',
            roof_evaluate: '',
            annex_evaluate:'',
            hall_evaluate:'',
            living_room_evaluate:'',
            dining_room_evaluate:'',
            conditioner_1:'',
            conditioner_2:'',
            conditioner_3:'',
            refrigerator:'',
            fraser:'',
            water_cooler:'',
            washing_machine:'',
            heater:'',
            cleaner:'',
            fireplace:'',
            fan:'',
            conditioner_1_evaluate:'',
            conditioner_2_evaluate:'',
            conditioner_3_evaluate:'',
            refrigerator_evaluate:'',
            fraser_evaluate:'',
            water_cooler_evaluate:'',
            washing_machine_evaluate:'',
            heater_evaluate:'',
            cleaner_evaluate:'',
            fireplace_evaluate:'',
            fan_evaluate:'',
            evaluate_furniture:'',
            furnished:'',
            home_category:[],
            category:'',
            electric:'',
          }
        }
      },
      error => {
        console.log(error);
      },
      () => {
      }
    );
  }

  logForm() {
    this.location_info.home_category = [];
    this.location_info.category = [];
    this.location_info.electric = [];
    this.get(this.home_category, this.location_info.home_category);
    this.get(this.category, this.location_info.category);
    this.electric();
    let data = {
      location_info: this.location_info
    }
    console.log(this.location_info.home_category)
    this.httpService.post(this.env.API_URL + 'auth/location', data).subscribe(
      data => {
        this.alertService.presentToast("تم حفظ البيانات بنجاح");
        console.log(data)
      },
      error => {
        this.alertService.presentToast(error);
      },
      () => {
        
      }
    )
  }

  public home_category = [
    { val: 'غرفة  نوم', isChecked: false },
    { val: 'صالة', isChecked: false },
    { val: 'حوش', isChecked: false },
    { val: 'مجلس', isChecked: false },
    { val: 'غرفة سائق', isChecked: false },
    { val: 'مطبخ', isChecked: false },
    { val: 'ملحق خارجي', isChecked: false },
    { val: 'صالة طعام', isChecked: false },
    { val: 'مستودع', isChecked: false }
  ];
  public category = [
    { val: 'أعمال كهربائية', isChecked: false },
    { val: 'أعمال سباكة', isChecked: false },
    { val: 'مطبخ', isChecked: false },
    { val: 'أبواب', isChecked: false },
    { val: 'حنفيات', isChecked: false },
    { val: 'شبابيك', isChecked: false },
    { val: 'صرف صحي', isChecked: false },
    { val: 'دهانات', isChecked: false },
    { val: 'تلبيس', isChecked: false },
    { val: 'لمبات', isChecked: false },
    { val: 'سخانات', isChecked: false },
    { val: 'شفاطات هواء', isChecked: false },
    { val: 'تكييف', isChecked: false },
    { val: 'ثلاجة', isChecked: false },
    { val: 'تمديدات كهرباء', isChecked: false },
    { val: 'إصلاح التسريبات', isChecked: false },
    { val: 'إصلاح التشققات', isChecked: false },
    { val: 'لايوجد', isChecked: false },
  ];
  electric(){
    switch (this.location_info.conditioner_1) {
      case '0':
        this.location_info.electric.push('مكيف سبيليت')
    }
    switch (this.location_info.conditioner_2) {
      case '0':
        this.location_info.electric.push('مكيف شبك')
    }
    switch (this.location_info.conditioner_3) {
      case '0':
        this.location_info.electric.push('مكيف صحراوي')
    }
    switch (this.location_info.refrigerator) {
      case '0':
        this.location_info.electric.push('ثلاجة')
    }
    switch (this.location_info.fraser) {
      case '0':
        this.location_info.electric.push('فريزر')
    }
    switch (this.location_info.water_cooler) {
      case '0':
        this.location_info.electric.push('برادة ماء')
    }
    switch (this.location_info.washing_machine) {
      case '0':
        this.location_info.electric.push('غسالة')
    }
    switch (this.location_info.heater) {
      case '0':
        this.location_info.electric.push('سخانة')
    }
    switch (this.location_info.cleaner) {
      case '0':
        this.location_info.electric.push('مكنسة كهرب')
    }
    switch (this.location_info.fireplace) {
      case '0':
        this.location_info.electric.push('دفاية')
    }
    switch (this.location_info.fan) {
      case '0':
        this.location_info.electric.push('مروحة شفط')
    }
    
  }
  // public electric = [
  //   { val: 'مكيف سبيليت', isChecked: false },
  //   { val: 'مكيف شبك', isChecked: false },
  //   { val: 'مكيف صحراوي', isChecked: false },
  //   { val: 'ثلاجة', isChecked: false },
  //   { val: 'فريزر', isChecked: false },
  //   { val: 'برادة ماء', isChecked: false },
  //   { val: 'غسالة', isChecked: false },
  //   { val: 'سخانة', isChecked: false },
  //   { val: 'مكنسة كهرب', isChecked: false },
  //   { val: 'دفاية', isChecked: false },
  //   { val: 'مروحة شفط', isChecked: false }
  //   ];
    
}