import { Component, OnInit } from '@angular/core';
import { AlertService } from 'src/app/services/alert.service';
import { AuthService } from 'src/app/services/auth.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { EnvService } from './../../../services/env.service';
import { Observable } from 'rxjs';
import{ HttpServiceService}  from './../../../services/http-service.service'
import { Commitments } from 'src/app/models/commitments';

@Component({
  selector: 'app-commitment',
  templateUrl: './commitment.page.html',
  styleUrls: ['./commitment.page.scss'],
})
export class CommitmentPage implements OnInit {
  commitments: Commitments;

  constructor(
    private alertService: AlertService,
    private authService :AuthService,
    private http : HttpClient,
    private env: EnvService,
    private httpService :HttpServiceService,

  ) { }

  ngOnInit() {
  }

  ionViewWillEnter() {
    this.httpService.get('auth/createcommit').subscribe(
      commitments => {
        this.commitments = commitments;
        if(!this.commitments){
          this.commitments = {
            rent_home:'',
            electricity_bill:'',
            water_bill:'' ,
            monthly_fees_eco_bank: '',
            monthly_fees: '',
          }
        }
      },
      error => {
        console.log(error);
      },
      () => {
        
      }
    );
  }


  logForm() {

    let data = {
      commitments: this.commitments
    }
    this.httpService.post(this.env.API_URL + 'auth/commitment', data).subscribe(
      data => {
        this.alertService.presentToast("تم حفظ البيانات بنجاح");
      },
      error => {
        console.log(error);
      },
      () => {
        
      }
    )

  
  }

}
