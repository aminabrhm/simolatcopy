import { Component, OnInit } from '@angular/core';
import { SoicalinfoService } from './../../../services/soicalinfo.service';
import { AlertService } from 'src/app/services/alert.service';
import { AuthService } from 'src/app/services/auth.service';
import { Subscription } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { EnvService } from './../../../services/env.service';
import * as moment from 'moment';
import { Socialinfo } from 'src/app/models/socialinfo';
import {HttpServiceService} from './../../../services/http-service.service'
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-social-info',
  templateUrl: './social-info.page.html',
  styleUrls: ['./social-info.page.scss'],
})
export class SocialInfoPage implements OnInit {

  social_info: Socialinfo ;
  constructor(
    private soicalInfoService: SoicalinfoService,
    private alertService: AlertService,
    private authService :AuthService,
    private http : HttpClient,
    private env: EnvService,
    private httpService: HttpServiceService

  ) { }

  ngOnInit() {
  }

  item_qty=[1];


  children_informations(form: NgForm){
    this.children_information.push(form.value.sex)
    this.children_information.push(form.value.name)
    this.children_information.push(form.value.national_id)
    this.children_information.push(form.value.birth_date)
    console.log(this.children_information)
  }
  add(){
    this.item_qty.push(1)
  }

  ionViewWillEnter() {
    this.httpService.get('auth/createsocial').subscribe(
      social_info => {
        this.social_info = social_info;
        if(!this.social_info){
          this.social_info = {
            disease: '',
            disease_informations: '',
            children_informations: '',
            social_status: '',
            family_number: '',
            wife_number: '',
            male_number: '',
            female_number: '',
            who_spend: '',
            other_person: '',
            other_person_num: '',
            other_person_reason: '',
            primary_school: '',
            middle_school: '',
            high_school: '',
            university: '',
            graduated: '',
          };
        }
      },
      error => {
        console.log(error);
      },
    );
  }
  children_information=[]


  logForm() {
    this.social_info.children_informations= this.children_information;
    // this.social_info.disease_date = moment(this.social_info.disease_date).format("YYYY-MM-DD");
    let data = {
      social_info: this.social_info
    }
    this.soicalInfoService.store(data).subscribe(
      data => {
        this.alertService.presentToast("تم حفظ البيانات بنجاح");
        console.log(data);
      },
      error => {
        console.log(error);
      },
      () => {
        
      }
    )
  }

}
