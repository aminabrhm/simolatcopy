import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SocialInfoPageRoutingModule } from './social-info-routing.module';

import { SocialInfoPage } from './social-info.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SocialInfoPageRoutingModule
  ],
  declarations: [SocialInfoPage]
})
export class SocialInfoPageModule {}
