import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ContactinfoService } from './../../../services/contactinfo.service';
import { AlertService } from 'src/app/services/alert.service'
import { Contact_info } from 'src/app/models/contact_info';
import {HttpServiceService} from './../../../services/http-service.service'
import { EnvService } from './../../../services/env.service';
import { initial_study } from 'src/app/models/initial_study';


@Component({
  selector: 'app-amina',
  templateUrl: './amina.page.html',
  styleUrls: ['./amina.page.scss'],
})
export class AminaPage implements OnInit {
  initial: initial_study;
  constructor(
    private alertService: AlertService,
    private env: EnvService,
    private httpService :HttpServiceService,
  ) { }
  

  ngOnInit() {

  }

  ionViewWillEnter() {
    this.httpService.get('auth/result').subscribe(
      initial => {
        this.initial = initial
      },
      error => {
        console.log(error);
      },
      () => {
        
      }
    );
  }
}

