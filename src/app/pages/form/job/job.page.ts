import { Component, OnInit } from '@angular/core';
import { AlertService } from 'src/app/services/alert.service';
import { EnvService } from './../../../services/env.service';
import{ HttpServiceService}  from './../../../services/http-service.service'
import { Job_info } from 'src/app/models/job_info';


@Component({
  selector: 'app-job',
  templateUrl: './job.page.html',
  styleUrls: ['./job.page.scss'],
})
export class JobPage implements OnInit {
  job_info: Job_info;

  constructor(
    private alertService: AlertService,
    private env: EnvService,
    private httpService :HttpServiceService,
  ) { }

  ngOnInit() {
  }

  ionViewWillEnter() {
    this.httpService.get('auth/createjob').subscribe(
      job_info => {
        this.job_info = job_info
        if(!this.job_info){
          this.job_info = {
            job: '',
            job_place:'',
            phone_number:'' ,
            education: '',
            salary: '',
            salary_month: '',
          }
        }
      },
      error => {
        console.log(error);
      },
      () => {
        
      }
    );
  }

  logForm() {
    let data = {
      job_info: this.job_info
    }
    this.httpService.post(this.env.API_URL + 'auth/jobInfo', data).subscribe(
      data => {
        this.alertService.presentToast("تم حفظ البيانات بنجاح");
      },
      error => {
        console.log(error);
      },
      () => {
        
      }
    )
  }

}
