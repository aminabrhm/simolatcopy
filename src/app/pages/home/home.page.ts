import { Component, OnInit } from '@angular/core';
import { NavController, ModalController } from '@ionic/angular';
import { User } from 'src/app/models/user';
import { AuthService } from 'src/app/services/auth.service';
import { AlertService } from 'src/app/services/alert.service';
import { initial_result } from 'src/app/models/initial_result';
import{ HttpServiceService}  from './../../services/http-service.service'

@Component({
  selector: 'app-home',
  templateUrl: './home.page.html',
  styleUrls: ['./home.page.scss'],
})

export class HomePage implements OnInit {

  user: User;
  initial:initial_result;

  constructor(
    private modalController: ModalController,
    private navCtrl: NavController,
    private authService: AuthService,
    private alertService: AlertService,
    private httpService :HttpServiceService,
  ) { }

  user_type: string;
  result: string;
  ngOnInit() {
  }
  
  ionViewWillEnter() {
    // this.authService.user().subscribe(
    //   user => {
    //     this.user = user;
    //     this.user_type = this.user.user_type
    //   }
    // );
    // this.httpService.get('auth/result').subscribe(
    //   initial => {
    //     this.initial = initial
    //     this.result = initial.result
    //   },
    //   error => {
    //     console.log(error);
    //   },
    //   () => {
    //   }
    // );
  }
  dismissPage() {
    this.modalController.dismiss();
  }

  logout() {
    this.authService.logout().subscribe(
      data => {
        this.alertService.presentToast("تم تسجيل الخروج");        
      },
      error => {
        console.log(error);
      },
      () => {
        this.navCtrl.navigateRoot('/login');
      }
    );
  }

}
