import { Component, OnInit } from '@angular/core';
import { ModalController, NavController } from '@ionic/angular';
import { NgForm } from '@angular/forms';
import { CommitteeLoginService } from 'src/app/services/committee-login.service';
import { AlertService } from 'src/app/services/alert.service';
import { Committee } from './../../..//models/committee';


@Component({
  selector: 'app-login-commit',
  templateUrl: './login-commit.page.html',
  styleUrls: ['./login-commit.page.scss'],
})
export class LoginCommitPage implements OnInit {

  constructor(
    private modalController: ModalController,
    private committeeLogin: CommitteeLoginService,
    private navCtrl: NavController,
    private alertService: AlertService  ) { }

  ngOnInit() {
  }
  dismissLogin() {
    this.modalController.dismiss();
  }

  login(form: NgForm) {
    this.committeeLogin.login(form.value.committees_number).subscribe(
      data => {
        this.alertService.presentToast(data +"Logged In");
      },
      error => {
      },
      () => {
        this.dismissLogin();
        this.navCtrl.navigateRoot('/show');
      }
    );
  }



  committees_number
}
