import { Component, OnInit } from '@angular/core';
import { ModalController, NavController } from '@ionic/angular';
import { RegisterPage } from '../register/register.page';
import { NgForm } from '@angular/forms';
import { AuthService } from 'src/app/services/auth.service';
import { AlertService } from 'src/app/services/alert.service';
import { LoginCommitPage } from '../login-commit/login-commit.page';



@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  constructor(
    private modalController: ModalController,
    private authService: AuthService,
    private navCtrl: NavController,
    private alertService: AlertService
  ) { }

  ngOnInit() {
  }
  checked : boolean = false;
	
  rememberMe(): void {
  	this.checked = !this.checked;
  	// console.log("checked: " + this.checked);//it is working !!!
  }
  dismissLogin() {
    this.modalController.dismiss();
  }
  async registerModal() {
    this.dismissLogin();
    const registerModal = await this.modalController.create({
      component: RegisterPage
    });
    return await registerModal.present();
  }

  login(form: NgForm) : void{

    // if(this.checked){
    //   window.localStorage.setItem ("username",form.value.email);
    //   window.localStorage.setItem("password",form.value.password);
    // }
    this.authService.login(form.value.email, form.value.password).subscribe(
      data => {
        localStorage.setItem('token', data.access_token);
        this.alertService.presentToast("تم تسجيل الدخول");
        console.log(data)
      },
      error => {
        this.alertService.presentToast("خطأ في اسم المستخدم أو كلمة المرور");
        console.log(error)
      },
      () => {
        this.dismissLogin();
        // this.navCtrl.navigateRoot('/initial-study');
        this.navCtrl.navigateRoot('/personal-info');
      }
    );
  }
  async loginCommit() {
    const loginCommitModal = await this.modalController.create({
      component: LoginCommitPage,
    });
    return await loginCommitModal.present();
  }

}
