import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { InitialStudyPageRoutingModule } from './initial-study-routing.module';

import { InitialStudyPage } from './initial-study.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    InitialStudyPageRoutingModule
  ],
  declarations: [InitialStudyPage]
})
export class InitialStudyPageModule {}
