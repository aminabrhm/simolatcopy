import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { InitialStudyPage } from './initial-study.page';

const routes: Routes = [
  {
    path: '',
    component: InitialStudyPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class InitialStudyPageRoutingModule {}
