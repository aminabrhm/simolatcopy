import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { InitialStudyPage } from './initial-study.page';

describe('InitialStudyPage', () => {
  let component: InitialStudyPage;
  let fixture: ComponentFixture<InitialStudyPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InitialStudyPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(InitialStudyPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
