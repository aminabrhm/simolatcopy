import { Component, OnInit } from '@angular/core';
import { AlertService } from 'src/app/services/alert.service';
import{ HttpServiceService}  from './../../services/http-service.service'
import { initial_study } from 'src/app/models/initial_study';
import { AuthService } from './../../services/auth.service';
import { Router } from '@angular/router'

@Component({
  selector: 'app-initial-study',
  templateUrl: './initial-study.page.html',
  styleUrls: ['./initial-study.page.scss'],
})
export class InitialStudyPage implements OnInit {
  initial: initial_study;
  constructor(
    private alertService: AlertService,
    private httpService :HttpServiceService,
    private authService: AuthService,
    private router: Router
  ) { }

  ngOnInit() {

  }
  ionViewWillEnter() {
    this.get();
  }

  logForm() {
    this.post()
  }


  get(): void {
    this.httpService.get('auth/result')
      .subscribe(initial => {
        console.log(initial);
        this.initial = initial;
      });
  }

  post(): void {
    let data = {
      initial: this.initial
    }
    this.httpService.post('auth/check_initialapi', data).subscribe(
      data => {
        this.alertService.presentToast("تم حفظ البيانات بنجاح");
      },
      error => {
        console.log(error);
      },
      () => {
        
      }
    )
  }
}
