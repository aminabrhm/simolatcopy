  export class Socialinfo {
    disease: string;
    disease_informations: string;
    children_informations: any;
    social_status:string;
    family_number: string
    wife_number: string;
    male_number: string
    female_number: string
    who_spend: string
    other_person: string
    other_person_num: string
    other_person_reason: string
    primary_school: string
    middle_school: string
    high_school: string
    university: string
    graduated: string
}