import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { tap } from 'rxjs/operators';
import { EnvService } from './env.service';
import { Committee } from '../models/committee';
import { NativeStorage } from '@ionic-native/native-storage/ngx';

@Injectable({
  providedIn: 'root'
})
export class CommitteeLoginService {


  constructor(
    private http: HttpClient,
    private storage: NativeStorage,
    private env: EnvService,
  ) { }


  login(committees_number: String) {
    let options = {headers: new HttpHeaders({ 
      'Content-Type': 'application/json',
     })}
     console.log(committees_number)
    return this.http.post(this.env.API_URL + 'auth/committee/check',
      committees_number,options).pipe(tap(
      res =>{
        console.log(res);
      },
      err => {
        console.log(err.message);
      }
    ))
  }
}