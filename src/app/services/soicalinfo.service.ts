import { Injectable } from '@angular/core';
import { HttpClient,HttpHeaders } from '@angular/common/http';
import { EnvService } from './env.service';
import { tap } from 'rxjs/operators';
import{ HttpServiceService}  from './http-service.service'
import{AuthService} from './auth.service'

@Injectable({
  providedIn: 'root'
})
export class SoicalinfoService {

  constructor(
    private http: HttpClient,
    private env: EnvService,
    private httpService :HttpServiceService,
    private authService: AuthService
  ) { }

 store( data : any ) {
  return this.httpService.post(this.env.API_URL + 'auth/information', data);
}
show() {
return this.httpService.get(this.env.API_URL + 'auth/information');
}
}
