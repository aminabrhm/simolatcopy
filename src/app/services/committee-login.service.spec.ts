import { TestBed } from '@angular/core/testing';

import { CommitteeLoginService } from './committee-login.service';

describe('CommitteeLoginService', () => {
  let service: CommitteeLoginService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CommitteeLoginService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
