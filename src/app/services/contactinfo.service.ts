import { Injectable } from '@angular/core';
import { HttpClient,HttpHeaders } from '@angular/common/http';
import { EnvService } from './env.service';
import { tap } from 'rxjs/operators';
import{ HttpServiceService}  from './http-service.service'



@Injectable({
  providedIn: 'root'
})
export class ContactinfoService {


  constructor(
    private http: HttpClient,
    private env: EnvService,
    private httpService :HttpServiceService,
  ) { }


 store( data : any ) {
    return this.httpService.post(this.env.API_URL + 'auth/contactInfo', data);
 }
 edit() {
  return this.httpService.get(this.env.API_URL + 'auth/contactInfo');
}

istore(email: String, phone_number: String, mobile_number: String, whatsapp_number: String ,other_number: String, relative: String){
  return  this.httpService.post(this.env.API_URL + 'auth/contactInfo',
  {email: email, phone_number: phone_number, mobile_number:mobile_number, whatsapp_number:whatsapp_number, other_number:other_number ,relative:relative}
  )
}
} 
