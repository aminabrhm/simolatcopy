import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError, tap } from 'rxjs/operators';
import { EnvService } from './env.service';
import { User } from '../models/user';
import { NativeStorage } from '@ionic-native/native-storage/ngx';
import { Observable, of } from 'rxjs';
// import { AnyAaaaRecord } from 'dns';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  isLoggedIn = false;
  token:any;
  headers = new HttpHeaders();
  constructor(
    private http: HttpClient,
    private storage: NativeStorage,
    private env: EnvService,
  ) { }
  
  login(email: String, password: String): Observable<any>{
    return this.http.post<any>(this.env.API_URL + 'auth/login',
      {email: email, password: password}
    ).pipe(
      tap(token => {
        this.storage.setItem('token', token.access_token)
        .then(
          () => {
            console.log('Token Stored');
            console.log(token)
          },
          error => console.error('Error storing item', error)
        );
        this.token = token;
        localStorage.setItem("token",token);
        this.isLoggedIn = true;
        console.log(token);
        return token;
      }),
      );
    }
  register(name: String, email: String, password: String, user_type: String ): Observable<any> {
    return this.http.post<any>(this.env.API_URL + 'auth/register',
      {name: name, email: email, password: password, user_type:user_type}
      ).pipe(
        tap(_ => this.log('register')),
        catchError(this.handleError('register', []))
      );
  }
  logout(): Observable<any>  {
    return this.http.get<any>(this.env.API_URL + 'auth/logout')
    .pipe(
      tap(_ => this.log('login')),
      catchError(this.handleError('login', []))
    );
  }
  user(): Observable<any>{
    return this.http.get<User>(this.env.API_URL + 'auth/user')
    .pipe(
      tap(user => {
        console.log(user)
        return user;
      })
    )
  }
  // get(endpoint: string): Observable<any>{
  //   return this.http.get<any>(this.env.API_URL + endpoint)
  //   .pipe(
  //     tap(data => {
  //       console.log(data)
  //       return data;
  //     })
  //   )
  // }
  get(endpoint: string): Observable<any> {
    // let options = {headers: new HttpHeaders({ 
    //   'Content-Type': 'application/json',
    //  })  };
    return this.http.get<any>(this.env.API_URL +endpoint)
      .pipe(
        tap(data => console.log(this.env.API_URL +endpoint)),
        catchError(this.handleError('getData', []))
      );
  }


  post(endpoint: string, data: any): Observable<any> {
    return this.http.post<any>(this.env.API_URL +endpoint, data)
      .pipe(
        tap(data => {
          console.log(data)
        })
      )
  }

  private log(message: string) {
    console.log(message);
  }

  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // TODO: better job of transforming error for user consumption
      this.log(`${operation} failed: ${error.message}`);

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }
}
