import { Injectable } from '@angular/core';
import { HttpClient ,HttpHeaders} from '@angular/common/http';
import { EnvService } from './env.service';
import { Contact_info } from 'src/app/models/contact_info';
import { Observable, of } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class HttpServiceService {

  headers = new HttpHeaders();
  isLoggedIn = false;

  constructor(
    private http : HttpClient,
    private env: EnvService,
    ) { }

  get(endpoint: string): Observable<any> {
    // let options = {headers: new HttpHeaders({ 
    //   'Content-Type': 'application/json',
    //  })  };
    return this.http.get<any>(this.env.API_URL +endpoint)
      .pipe(
        tap(data => console.log(this.env.API_URL +endpoint)),
        catchError(this.handleError('getData', []))
      );
  }
  post(endpoint: string, data: any): Observable<any> {
    // let options = {headers: new HttpHeaders({ 
    //   'Content-Type': 'application/json',
    //  })  };
    return this.http.post<any>(this.env.API_URL +endpoint, data)
  }

  contact() {
    console.log("Hi I'm contact")
    return this.http.get<Contact_info>(this.env.API_URL + 'auth/createcontact')
    .pipe(
      tap(
        contact_info => {
        return contact_info
        }
      )
    )
  }

  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      console.log(error); // log to console instead

      // TODO: better job of transforming error for user consumption
      this.log(`${operation} failed: ${error.message}`);

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }

  /** Log a HeroService message with the MessageService */
  private log(message: string) {
    console.log(message);
  }

  
}
